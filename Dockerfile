FROM mono:6

RUN useradd -d /home/container -m container
RUN apt update
RUN apt install -y unzip wget

WORKDIR /home/container

COPY ./entrypoint.sh /entrypoint.sh

CMD ["/bin/bash", "/entrypoint.sh"]